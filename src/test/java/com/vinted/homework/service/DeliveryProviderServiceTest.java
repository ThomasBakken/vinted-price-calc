package com.vinted.homework.service;

import com.vinted.homework.TestBase;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.DeliveryProviderRepository;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.service.io.DeliveryProviderReaderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryProviderServiceTest extends TestBase {
    private DeliveryProviderService deliveryProviderService;

    @Mock
    DeliveryProviderReaderService deliveryProviderReaderService;

    @Mock
    DeliveryProviderRepository deliveryProviderRepository;

    private List<DeliveryProvider> sampleDeliveryProviders;

    @Before
    public void setup() throws IOException {
        String defaultProviderResourceFile = DeliveryProviderService.DEFAULT_PROVIDER_FILENAME;
        sampleDeliveryProviders = getSampleDeliveryProviders();

        when(deliveryProviderReaderService.load(defaultProviderResourceFile)).thenReturn(sampleDeliveryProviders);
        when(deliveryProviderRepository.findAll()).thenReturn(sampleDeliveryProviders);

        deliveryProviderService = new DeliveryProviderService(deliveryProviderReaderService, deliveryProviderRepository);
    }

    @Test
    public void getDeliveryProvidersTest() {
        List<DeliveryProvider> result = deliveryProviderService.findAll();
        assertThat(result, is(sampleDeliveryProviders));
    }

    @Test
    public void getDeliveryProviderByNameTest() {
        DeliveryProvider result = deliveryProviderService.findByName("MR").get();
        assertThat(result, notNullValue());
        assertThat(result.getName(), is("MR"));
    }

    @Test
    public void findBestOfferTest() {
        DeliveryProvider bestOffer = deliveryProviderService.findBestOffer(PackageSize.S);
        assertThat(bestOffer.getName(), is("LP"));

        bestOffer = deliveryProviderService.findBestOffer(PackageSize.L);
        assertThat(bestOffer.getName(), is("MR"));
    }
}
