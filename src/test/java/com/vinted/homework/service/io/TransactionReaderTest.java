package com.vinted.homework.service.io;

import com.vinted.homework.TestBase;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.service.DeliveryProviderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionReaderTest extends TestBase {

    private String filename = "/single-transaction.resource";
    private TransactionReaderService transactionReaderService;

    @Mock
    private FileReaderService fileReaderService;

    @Mock
    private DeliveryProviderService deliveryProviderService;

    @Before
    public void setup() throws Exception {

        transactionReaderService = new TransactionReaderService(fileReaderService, deliveryProviderService);
    }

    @Test
    public void parseSampleTransactionFile() throws Exception {
        String contents = readResourceFile(filename);
        when(fileReaderService.readFile(filename)).thenReturn(contents);
        when(deliveryProviderService.findByName("MR")).thenReturn(Optional.of(getMRDeliveryProvider()));

        List<Transaction> result = transactionReaderService.loadFromFile(filename);

        verify(fileReaderService, times(1)).readFile(filename);

        assertThat(result, hasSize(1));
        Transaction entry = result.get(0);

        LocalDate expectedDate = LocalDate.parse("2015-02-01");
        PackageSize expectedPackageSize = PackageSize.S;
        DeliveryProvider expectedDeliveryProvider = getMRDeliveryProvider();

        assertThat(entry.getLocalDate(), is(expectedDate));
        assertThat(entry.getDeliveryProvider().getName(), is(expectedDeliveryProvider.getName()));
        assertThat(entry.getPackageSize(), is(expectedPackageSize));
    }

    @Test
    public void invalidDateInTransactionFileTest() {
        String invalidDateTransaction = "102-19-02 S MR";
        List<Transaction> result = transactionReaderService.parseTransactions(invalidDateTransaction);
        assertThat(result, hasSize(1));
        assertThat(result.get(0).isValid(), is(false));
        assertThat(result.get(0).getRaw(), is(invalidDateTransaction));
    }

    @Test
    public void unknownProviderNameTest() {
        String unknownProviderRaw = "102-19-02 S FOO";
        List<Transaction> result = transactionReaderService.parseTransactions(unknownProviderRaw);
        assertThat(result, hasSize(1));
        assertThat(result.get(0).isValid(), is(false));
        assertThat(result.get(0).getRaw(), is(unknownProviderRaw));
    }
}
