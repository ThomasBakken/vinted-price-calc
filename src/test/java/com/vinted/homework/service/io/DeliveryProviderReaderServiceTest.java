package com.vinted.homework.service.io;

import com.vinted.homework.TestBase;
import com.vinted.homework.repository.entity.DeliveryProvider;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DeliveryProviderReaderServiceTest extends TestBase {

    private String resourceFile = "/providers.resource";
    private DeliveryProviderReaderService deliveryProviderReaderService = new DeliveryProviderReaderService();

    @Test
    public void parseResourceFileSuccessTest() throws Exception {
        String contents = this.readResourceFile(resourceFile);

        assertTrue("Contents of provider resource file is null", contents != null);
        assertTrue("Provider resource file is empty", contents.length() > 0);

        List<DeliveryProvider> deliveryProviders = deliveryProviderReaderService.parse(contents);
        assertTrue("Expected 2 providers, but found " + deliveryProviders.size(), deliveryProviders.size() == 2);
    }
}
