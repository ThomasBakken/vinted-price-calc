package com.vinted.homework;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.DeliveryMethod;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class TestBase {
    private final String defaultDelimiter = "\\z"; //EOF delimiter

    protected String readResourceFile(String path) throws Exception {
        return this.readResourceFile(path, this.defaultDelimiter);
    }

    protected String readResourceFile(String path, String delimiter) throws Exception {
        InputStream inputStream  = getClass().getResourceAsStream(path);
        if (inputStream == null) {
            throw new IOException("Resource "  + path + " not found");
        }

        String content;
        try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name()) ) {
            content = scanner.useDelimiter(delimiter).next();
        }

        inputStream.close();;

        return content;
    }

    protected List<DeliveryProvider> getSampleDeliveryProviders() {
        List<DeliveryProvider> providers = new ArrayList<>();
        providers.add(getMRDeliveryProvider());
        providers.add(getLPDeliveryProvider());
        return providers;
    }

    protected DeliveryProvider getMRDeliveryProvider() {
        String name = "MR";
        List<DeliveryMethod> methods = createDeliveryMethods(2.00d, 3.00d, 4.00d);
        return new DeliveryProvider(name, methods);
    }

    protected DeliveryProvider getLPDeliveryProvider() {
        String name = "LP";
        List<DeliveryMethod> methods = createDeliveryMethods(1.50d, 4.90d, 6.90d);
        return new DeliveryProvider(name, methods);
    }

    protected DeliveryProvider createDeliveryProvider(String name, List<DeliveryMethod> deliveryMethods) {
        return new DeliveryProvider(name, deliveryMethods);
    }

    private List<DeliveryMethod> createDeliveryMethods(double smallPrice, double mediumPrice, double largePrice) {
        List<DeliveryMethod> methods = new ArrayList<>();
        methods.add(new DeliveryMethod(new Price(smallPrice), PackageSize.S));
        methods.add(new DeliveryMethod(new Price(mediumPrice), PackageSize.M));
        methods.add(new DeliveryMethod(new Price(largePrice), PackageSize.L));

        return methods;
    }

    protected List<TransactionHistory> createTransactionHistory(int amount, DeliveryProvider provider, PackageSize size, LocalDate date) {
        List<TransactionHistory> entries = new ArrayList<>();
        for(int i = 0; i < amount; i++) {
            Transaction t = createTransaction(provider, size, date);
            TransactionHistory entry  = new TransactionHistory();
            entry.setOriginalTransaction(t);
            entry.setTotalDiscount(new Price(0d));
            entries.add(entry);
        }

        return entries;
    }

    protected Transaction createTransaction(DeliveryProvider provider, PackageSize packageSize, LocalDate localDate) {
        Transaction t = new Transaction();
        t.setValid(true);
        t.setLocalDate(localDate);
        t.setPackageSize(packageSize);
        t.setDeliveryProvider(provider);
        t.setValid(true);
        return t;
    }
}
