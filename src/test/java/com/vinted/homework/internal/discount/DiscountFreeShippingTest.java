package com.vinted.homework.internal.discount;

import com.vinted.homework.TestBase;
import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;
import com.vinted.homework.service.TransactionHistoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DiscountFreeShippingTest extends TestBase {

    @Mock
    private TransactionHistoryService transactionHistoryService;

    @Test
    public void freeShippingDefaultConfigurationSuccessTest() {
        FreeShipping freeShipping = new FreeShipping(transactionHistoryService);

        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        transaction.setOriginalPrice(new Price(6.90d));

        List<TransactionHistory> twoEntriesTransactionHistory =
            createTransactionHistory(2, getLPDeliveryProvider(), PackageSize.L, date);
        when(transactionHistoryService.findByYearAndMonth(date)).thenReturn(twoEntriesTransactionHistory);

        boolean result = freeShipping.applicable(transaction);
        assertThat(result, equalTo(true));

        freeShipping.process(transaction);
        assertThat("Number of discounts does not match the expected",
            transaction.getDiscounts().size(), equalTo(1));

        Discount d = transaction.getDiscounts().get(0);
        double expectedDiscount = transaction.getOriginalPrice().getAmount();
        assertThat("Discount price not the same as original price",
            d.getPrice().getAmount(), closeTo(expectedDiscount, 0.1d));
    }

    @Test(expected=IllegalArgumentException.class)
    public void freeShippingDefaultConfigurationNotApplicableTest() {
        FreeShipping freeShipping = new FreeShipping(transactionHistoryService);

        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        transaction.setOriginalPrice(new Price(6.90d));

        List<TransactionHistory> singleEntry =
            createTransactionHistory(1, getLPDeliveryProvider(), PackageSize.L, date);
        when(transactionHistoryService.findByYearAndMonth(date)).thenReturn(singleEntry);

        boolean result = freeShipping.applicable(transaction);
        assertThat(result, equalTo(false));

        //Throws illegal argument exception due to not being applicable
        freeShipping.process(transaction);
    }

    @Test(expected=IllegalArgumentException.class)
    public void freeShippingDifferentProviderFailsTest() {
        FreeShipping freeShipping = new FreeShipping(transactionHistoryService);

        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        transaction.setOriginalPrice(new Price(6.90d));

        List<TransactionHistory> singleEntry =
            createTransactionHistory(2, getMRDeliveryProvider(), PackageSize.L, date);
        when(transactionHistoryService.findByYearAndMonth(date)).thenReturn(singleEntry);

        boolean result = freeShipping.applicable(transaction);
        assertThat(result, equalTo(false));

        //Throws illegal argument exception due to not being applicable
        freeShipping.process(transaction);
    }

    @Test
    public void freeShippingTenthShipmentTest() {
        FreeShipping freeShipping = new FreeShipping(transactionHistoryService, PackageSize.M,
            "MR", 10);

        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getMRDeliveryProvider(), PackageSize.M, date);
        transaction.setOriginalPrice(new Price(4.00d));

        List<TransactionHistory> singleEntry =
            createTransactionHistory(9, getMRDeliveryProvider(), PackageSize.M, date);
        when(transactionHistoryService.findByYearAndMonth(date)).thenReturn(singleEntry);

        boolean result = freeShipping.applicable(transaction);
        assertThat(result, equalTo(true));

        //Throws illegal argument exception due to not being applicable
        freeShipping.process(transaction);
        assertThat("Number of discounts does not match the expected",
            transaction.getDiscounts().size(), equalTo(1));

        Discount d = transaction.getDiscounts().get(0);
        double expectedDiscount = transaction.getOriginalPrice().getAmount();
        assertThat("Discount price not the same as original price",
            d.getPrice().getAmount(), closeTo(expectedDiscount, 0.1d));
    }
}
