package com.vinted.homework.internal.discount;

import com.vinted.homework.TestBase;
import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.service.DeliveryProviderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PackageSizeDiscountRuleTests extends TestBase {

    @Mock
    private DeliveryProviderService deliveryProviderService;

    @Test
    public void packageSizeSmallDiscountSuccessTest() {
        PackageSizeDiscountRule psDiscountRule = new PackageSizeDiscountRule(deliveryProviderService, PackageSize.S);

        Transaction transaction = new Transaction();
        transaction.setValid(true);
        transaction.setPackageSize(PackageSize.S);
        transaction.setDeliveryProvider(getMRDeliveryProvider());

        when(deliveryProviderService.findBestOffer(PackageSize.S)).thenReturn(getLPDeliveryProvider());
        assertThat(psDiscountRule.applicable(transaction), equalTo(true));

        transaction.setOriginalPrice(new Price(2.00));
        psDiscountRule.process(transaction);

        assertThat(transaction.getDiscounts().size(), equalTo(1));

        Price expectedDiscount = new Price(.50d);
        Discount discount = transaction.getDiscounts().get(0);
        assertThat(discount.getPrice().getAmount(), closeTo(expectedDiscount.getAmount(), 0.1d));
    }

    @Test(expected=IllegalArgumentException.class)
    public void packageMediumNoDiscountTest() {
        PackageSizeDiscountRule psDiscountRule = new PackageSizeDiscountRule(deliveryProviderService, PackageSize.S);

        Transaction transaction = new Transaction();
        transaction.setValid(true);
        transaction.setPackageSize(PackageSize.M);
        transaction.setDeliveryProvider(getMRDeliveryProvider());

        assertThat(psDiscountRule.applicable(transaction), equalTo(false));

        //We also want to test that the discount rule refuses to process the transaction
        //This will trigger an illegal argument exception
        transaction.setOriginalPrice(new Price(2.00));
        psDiscountRule.process(transaction);
    }

    @Test()
    public void packageMediumDiscountRuleSuccessTest() {
        PackageSizeDiscountRule psDiscountRule = new PackageSizeDiscountRule(deliveryProviderService, PackageSize.M);

        Transaction transaction = new Transaction();
        transaction.setValid(true);
        transaction.setPackageSize(PackageSize.M);
        transaction.setDeliveryProvider(getLPDeliveryProvider());

        when(deliveryProviderService.findBestOffer(PackageSize.M)).thenReturn(getMRDeliveryProvider());
        assertThat(psDiscountRule.applicable(transaction), equalTo(true));

        transaction.setOriginalPrice(new Price(4.90));
        psDiscountRule.process(transaction);

        assertThat(transaction.getDiscounts().size(), equalTo(1));

        Price expectedDiscount = new Price(1.90);
        Discount discount = transaction.getDiscounts().get(0);
        assertThat(discount.getPrice().getAmount(), closeTo(expectedDiscount.getAmount(), 0.1));
    }
}
