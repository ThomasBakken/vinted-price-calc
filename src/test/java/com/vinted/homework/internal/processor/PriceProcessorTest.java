package com.vinted.homework.internal.processor;

import com.vinted.homework.TestBase;
import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

public class PriceProcessorTest extends TestBase {

    @Test
    public void calculateGrandTotalsNoDiscountTest() {
        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction t = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        Price expected = new Price(5.00d);
        t.setOriginalPrice(expected);

        List<TransactionHistory> historyEntries = Collections.emptyList();

        TransactionHistory result = PriceProcessor.calculateGrandTotals(t, historyEntries);
        assertThat("Grand totals does not match expected", result.getFinalPrice().getAmount(),
            closeTo(expected.getAmount(), 0.1d));
    }

    @Test
    public void calculateGrandTotalsDiscountAppliedTest() {
        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        Price originalPrice = new Price(5.00d);
        transaction.setOriginalPrice(originalPrice);

        double discountAmount = 3.00d;
        Discount d = new Discount(new Price(discountAmount), "test discount");
        transaction.addDiscount(d);

        List<TransactionHistory> historyEntries = Collections.emptyList();

        double expectedFinalPrice = originalPrice.getAmount() - discountAmount;
        TransactionHistory result = PriceProcessor.calculateGrandTotals(transaction, historyEntries);
        assertThat("Grand totals does not match expected", result.getFinalPrice().getAmount(),
            closeTo(expectedFinalPrice, 0.10d));

        assertThat("Discounted amount does not match the discount", result.getTotalDiscount().getAmount(),
            closeTo(discountAmount, 0.10d));
    }

    @Test
    public void calculateGrandTotalsDiscountExceedsMaxAmountTest() {
        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        Price originalPrice = new Price(6.90d);
        transaction.setOriginalPrice(originalPrice);

        double discountAmount = 2.00d;
        Discount d = new Discount(new Price(discountAmount), "test discount");
        transaction.addDiscount(d);

        List<TransactionHistory> historyEntries = createTransactionHistory(6, getLPDeliveryProvider(),
            PackageSize.M, date);

        TransactionHistory historyEntry = historyEntries.get(0);
        historyEntry.setTotalDiscount(new Price(10.00d)); //Equals default max discount

        TransactionHistory result = PriceProcessor.calculateGrandTotals(transaction, historyEntries);
        assertThat(result.getFinalPrice().getAmount(), closeTo(originalPrice.getAmount(), 0.10d));
    }

    @Test
    public void calculateGrandTotalsPartialDiscountTest() {
        LocalDate date = LocalDate.parse("2000-01-01");
        Transaction transaction = createTransaction(getLPDeliveryProvider(), PackageSize.L, date);
        Price originalPrice = new Price(6.90d);
        transaction.setOriginalPrice(originalPrice);

        double discountAmount = 2.00d;
        Discount d = new Discount(new Price(discountAmount), "test discount");
        transaction.addDiscount(d);

        List<TransactionHistory> historyEntries = createTransactionHistory(1, getLPDeliveryProvider(),
            PackageSize.L, date);

        double accumulatedDiscount = 8.50d;
        TransactionHistory historyEntry = historyEntries.get(0);
        historyEntry.setTotalDiscount(new Price(accumulatedDiscount));

        double expectedDiscount = 10.0d - accumulatedDiscount; //1.5
        TransactionHistory result = PriceProcessor.calculateGrandTotals(transaction, historyEntries);
        assertThat(result.getTotalDiscount().getAmount(), closeTo(expectedDiscount, 0.10d));

        double finalPrice = result.getFinalPrice().getAmount();
        double expectedFinalPrice = originalPrice.getAmount() - expectedDiscount;
        assertThat(finalPrice, closeTo(expectedFinalPrice, 0.10d));
    }
}
