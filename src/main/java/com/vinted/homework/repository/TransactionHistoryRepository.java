package com.vinted.homework.repository;

import com.vinted.homework.repository.entity.TransactionHistory;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionHistoryRepository extends Repository<TransactionHistory> {
    public List<TransactionHistory> findByYearAndMonth(LocalDate current) {
        return findAll().stream()
            .filter(transHistory -> {
                boolean isCurrentYear = transHistory.getOriginalTransaction()
                    .getLocalDate().getYear() == current.getYear();

                boolean isCurrentMonth = transHistory.getOriginalTransaction().getLocalDate()
                    .getMonth() == current.getMonth();

                return isCurrentYear && isCurrentMonth;
            }).collect(Collectors.toList());
    }
}
