package com.vinted.homework.repository.entity;

import com.vinted.homework.domain.delivery.DeliveryMethod;
import com.vinted.homework.domain.delivery.PackageSize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryProvider extends Entity {
    private final Map<PackageSize, DeliveryMethod> deliveryMethods;
    private final String name;

    public DeliveryProvider(String name, List<DeliveryMethod> deliveryMethods) {
        this.name = name;
        this.deliveryMethods = new HashMap<>();

        if(deliveryMethods == null) {
            return;
        }

        deliveryMethods.stream().forEach(dm ->
            this.deliveryMethods.put(dm.getSize(), dm)
        );
    }

    public DeliveryProvider(String name, Map<PackageSize, DeliveryMethod> deliveryMethods) {
        this.name = name;
        if(deliveryMethods == null) {
            this.deliveryMethods = new HashMap<>();
            return;
        }

        this.deliveryMethods = deliveryMethods;
    }

    public Map<PackageSize, DeliveryMethod> getDeliveryMethods() {
        return deliveryMethods;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "DeliveryProvider {" +
            "name='" + name + '\'' +
            ", deliveryMethods=" + deliveryMethods +
            '}';
    }

    /**
     * Compare two delivery providers.
     * Assumption: two delivery providers cannot have the same name
     * @param other the delivery provider to compare with
     * @return true if the delivery providers are the same (i.e. have the same name)
     */
    public boolean equals(DeliveryProvider other) {
        return this.name.equals(other.getName());
    }
}
