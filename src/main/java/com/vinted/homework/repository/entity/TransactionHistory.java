package com.vinted.homework.repository.entity;

import com.vinted.homework.domain.DiscountLineItem;
import com.vinted.homework.domain.Price;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistory extends Entity {
    private List<DiscountLineItem> discounts = new ArrayList<>();
    private Transaction originalTransaction;
    private Price finalPrice;
    private Price totalDiscount;

    public List<DiscountLineItem> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<DiscountLineItem> discounts) {
        this.discounts = discounts;
    }

    public void addDiscountLineItem(DiscountLineItem lineItem) {
        discounts.add(lineItem);
    }

    public Transaction getOriginalTransaction() {
        return originalTransaction;
    }

    public void setOriginalTransaction(Transaction originalTransaction) {
        this.originalTransaction = originalTransaction;
    }

    public Price getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Price finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Price getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Price totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    @Override
    public String toString() {
        return "TransactionHistory{" +
            "originalTransaction=" + originalTransaction +
            ", finalPrice=" + finalPrice +
            ", totalDiscount=" + totalDiscount +
            '}';
    }
}
