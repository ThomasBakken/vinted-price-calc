package com.vinted.homework.repository.entity;

import java.util.UUID;

public abstract class Entity  {
    private UUID id;

    public UUID getId(){
        return id;
    };

    public void setId(UUID id) {
        assert(id != null);
        this.id = id;
    }
}
