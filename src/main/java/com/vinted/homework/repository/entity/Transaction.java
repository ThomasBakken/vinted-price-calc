package com.vinted.homework.repository.entity;

import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Transaction extends Entity {
    private long lineNumber;
    private boolean valid;
    private String raw;
    private LocalDate localDate;
    private PackageSize packageSize;
    private Price originalPrice;

    private DeliveryProvider deliveryProvider;
    private List<Discount> discounts = new ArrayList<>();

    public Transaction() {
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public Price getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Price originalPrice) {
        this.originalPrice = originalPrice;
    }

    public PackageSize getPackageSize() {
        return packageSize;
    }

    public void setPackageSize(PackageSize packageSize) {
        this.packageSize = packageSize;
    }

    public DeliveryProvider getDeliveryProvider() {
        return deliveryProvider;
    }

    public void setDeliveryProvider(DeliveryProvider deliveryProvider) {
        this.deliveryProvider = deliveryProvider;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void addDiscount(Discount discount) {
        discounts.add(discount);
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long  getLineNumber() {
        return lineNumber;
    }

    @Override
    public String toString() {
        return "Transaction { " +
            "raw='" + raw + '\'' +
            '}';
    }

}
