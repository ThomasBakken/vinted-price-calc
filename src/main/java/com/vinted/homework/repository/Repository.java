package com.vinted.homework.repository;

import com.vinted.homework.repository.entity.Entity;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Repository<T extends Entity> {
    private Map<UUID, T> inMemoryRepo =  new HashMap<>();

    public T save(T entry) {
        if(entry.getId() == null) {
            UUID id = UUID.randomUUID();
            entry.setId(id);
        }

        this.inMemoryRepo.put(entry.getId(), entry);

        return entry;
    };


    public List<T> save(List<T> entries) {
        if(entries.isEmpty()) return Collections.emptyList();

        entries.stream().forEachOrdered(entry -> save(entry));

        return findAll();
    };

    public List<T> findAll() {
        return this.inMemoryRepo.entrySet()
            .stream()
            .map(entry -> entry.getValue())
            .collect(Collectors.toList());
    }

    public T find(UUID id) {
        return inMemoryRepo.get(id);
    }
}
