package com.vinted.homework.domain;

import com.vinted.homework.internal.logging.Loggable;

import java.util.Currency;

public class Price extends Loggable implements Comparable<Price>  {
    private final double amount;
    private final Currency currency;

    /**
     * Create a new price with in the given currency code
     * @param amount The Amount
     * @param currencyCode ISO 4217 currency code
     */
    public Price(double amount, String currencyCode) {
        this.amount = amount;
        this.currency = Currency.getInstance(currencyCode);
    }

    /**
     * Create a new price with default currency EUR
     * @param amount The amount
     */
    public Price(double amount) {
        this(amount, "EUR");
    }

    public boolean moreThan(Price other) {
        return compareTo(other) == 1;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return String.format ("%.2f %s", this.amount, this.currency.getSymbol());
    }

    @Override
    public int compareTo(Price other) {
        if(this.currency != other.currency) {
            logger.error("Cannot compare price of two different currencies. I was: %s, other was %s",
                this.currency.getCurrencyCode(), other.currency.getCurrencyCode());
            throw new UnsupportedOperationException("Cannot compare two different currencies!");
        }

        if(this.amount > other.amount) {
            return 1;
        }
        if(this.amount < other.amount) {
            return -1;
        }

        return 0;
    }
}
