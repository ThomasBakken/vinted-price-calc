package com.vinted.homework.domain;

public class DiscountLineItem  {
    private final Discount discount;

    public DiscountLineItem(Discount discount) {
        this.discount = discount;
    }

    public Discount getDiscount() {
        return discount;
    }
}
