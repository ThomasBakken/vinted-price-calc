package com.vinted.homework.domain;

public class Discount {
    private final Price price;
    private final String description;

    public Discount(Price price, String description) {
        assert price != null;
        this.price = price;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public Price getPrice() {
        return price;
    }
}
