package com.vinted.homework.domain.delivery;

import com.vinted.homework.domain.Price;

public class DeliveryMethod {
    private final Price price;
    private final PackageSize size;

    public DeliveryMethod(final Price price, final PackageSize size) {
        this.price = price;
        this.size = size;
    }

    public Price getPrice() {
        return price;
    }

    public PackageSize getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "DeliveryMethod{" +
            "price=" + price +
            ", size=" + size +
            '}';
    }
}
