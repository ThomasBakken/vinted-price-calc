package com.vinted.homework.domain.delivery;

public enum PackageSize {
    S,
    M,
    L
}
