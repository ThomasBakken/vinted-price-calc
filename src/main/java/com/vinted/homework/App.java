package com.vinted.homework;

import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.internal.discount.DiscountRule;
import com.vinted.homework.internal.discount.FreeShipping;
import com.vinted.homework.internal.discount.PackageSizeDiscountRule;
import com.vinted.homework.internal.processor.TransactionProcessor;
import com.vinted.homework.repository.DeliveryProviderRepository;
import com.vinted.homework.repository.DiscountRuleRepository;
import com.vinted.homework.repository.TransactionHistoryRepository;
import com.vinted.homework.repository.TransactionRepository;
import com.vinted.homework.repository.entity.TransactionHistory;
import com.vinted.homework.service.*;

import java.util.Comparator;
import java.util.List;

public class App {
    private RepositoryService repoService;
    private DeliveryProviderService deliveryProviderService;
    private TransactionHistoryService transactionHistoryService;
    private DiscountService discountService;
    private TransactionService transactionService;

    public static void main(String[] args) {
        String inputFile = null;

        if (args.length > 0) {
            inputFile = args[0];
        } else {
            printHelp();
            System.exit(1);
        }

        new App(inputFile);
    }

    public App(String transactionFile) {
        //Initialize all services
        loadServices(transactionFile);

        //Initialize hard coded discount rules
        initDefaultDiscountRules(discountService, transactionHistoryService, deliveryProviderService);

        TransactionProcessor transactionProcessor = new TransactionProcessor(transactionService,
            transactionHistoryService, discountService);

        //Run the transaction processor on all transactions
        transactionProcessor.processAll();

        //Output log
        printResult();
    }

    private void loadServices(String transactionFile) {
        repoService = new RepositoryService();

        DeliveryProviderRepository deliveryProviderRepository = repoService.get(DeliveryProviderRepository.class);
        deliveryProviderService = new DeliveryProviderService(deliveryProviderRepository);

        TransactionRepository transactionRepository = repoService.get(TransactionRepository.class);
        transactionService = new TransactionService(deliveryProviderService, transactionRepository, transactionFile);

        TransactionHistoryRepository transactionHistoryRepository = new TransactionHistoryRepository();
        transactionHistoryService = new TransactionHistoryService(transactionHistoryRepository);

        DiscountRuleRepository discountRuleRepository = repoService.get(DiscountRuleRepository.class);
        discountService = new DiscountService(discountRuleRepository);
    }

    private void initDefaultDiscountRules(DiscountService discountService,
                                          TransactionHistoryService transactionHistoryService,
                                          DeliveryProviderService deliveryProviderService) {
        DiscountRule smallPackageDiscount =  new PackageSizeDiscountRule(deliveryProviderService, PackageSize.S);
        DiscountRule thirdShipmentFree = new FreeShipping(transactionHistoryService, FreeShipping.DEFAULT_PACKAGE_SIZE,
            FreeShipping.DEFAULT_PROVIDER_NAME, FreeShipping.DEFAULT_SHIPMENT_NUMBER);

        discountService.addDiscountRule(smallPackageDiscount);
        discountService.addDiscountRule(thirdShipmentFree);
    }

    private void printResult() {
        List<TransactionHistory> processed = transactionHistoryService.findAll();
        processed.sort(Comparator.comparing(a -> a.getOriginalTransaction().getLineNumber()));

        for(TransactionHistory entry : processed) {
            if(!entry.getOriginalTransaction().isValid()) {
                System.out.println(entry.getOriginalTransaction().getRaw() + " IGNORED");
                continue;
            }

            String discount = "-";
            if(entry.getTotalDiscount().getAmount() > 0.00) {
                discount = String.format("%.2f", entry.getTotalDiscount().getAmount());
            }

            String output = String.format("%s %.2f %s", entry.getOriginalTransaction().getRaw(),
                entry.getFinalPrice().getAmount(), discount);

            System.out.println(output);
        }
    }

    private static void printHelp() {
        String example = "java -jar discount-calc.jar %s";
        System.out.println("Missing argument. Aborting");

        System.out.print("Usage: ");
        System.out.println(String.format(example, "<transaction-input-file>"));

        System.out.print("Example: ");
        System.out.println(String.format(example, "input.txt"));
    }
}
