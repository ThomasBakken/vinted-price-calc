package com.vinted.homework.internal.discount;

import com.vinted.homework.repository.entity.Entity;
import com.vinted.homework.repository.entity.Transaction;

public abstract class DiscountRule<T extends Transaction> extends Entity {

    /**
     * Condition to evaluate if this discount rule applies to a transaction
     * @param transaction The transaction (or a subclass of transaction) to evaluate
     * @return true if and only if
     */
    public abstract boolean applicable(T transaction);


    /**
     * Apply a discount to a transaction.
     * Any discount applied is added as a separate line item to the transaction, in order to verify for example discount
     * limits at a later point in time.
     * @param transaction The transaction to apply a discount to
     * @return transaction with the discount applied, if the applicable condition was met
     */
    public abstract void  process(T transaction);
}
