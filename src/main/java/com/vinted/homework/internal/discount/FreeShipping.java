package com.vinted.homework.internal.discount;

import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;
import com.vinted.homework.service.TransactionHistoryService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class FreeShipping extends DiscountRule<Transaction> {
    public static int DEFAULT_SHIPMENT_NUMBER = 3;
    public static String DEFAULT_PROVIDER_NAME = "LP";
    public static PackageSize DEFAULT_PACKAGE_SIZE = PackageSize.L;

    private final TransactionHistoryService transactionHistoryService;
    private final String name = "Free shipping";

    private final PackageSize packageSize;
    private final String providerName;
    private final int freeShipmentNumber;

    public FreeShipping(TransactionHistoryService transactionHistoryService) {
        this(transactionHistoryService, FreeShipping.DEFAULT_PACKAGE_SIZE, FreeShipping.DEFAULT_PROVIDER_NAME,
            FreeShipping.DEFAULT_SHIPMENT_NUMBER);
    }

    public FreeShipping(TransactionHistoryService transactionHistoryService, PackageSize packageSize,
                        String providerName, int shipmentNumber) {
        this.transactionHistoryService = transactionHistoryService;
        this.packageSize = packageSize;
        this.providerName = providerName;

        if(shipmentNumber < 1) {
            //A bit of safeguarding
            this.freeShipmentNumber = 1;
        } else {
            this.freeShipmentNumber = shipmentNumber;
        }
    }

    public boolean applicable(Transaction transaction) {
        if(transaction.getPackageSize() != packageSize) {
            return false;
        }

        if(!transaction.getDeliveryProvider().getName().equals(providerName)) {
            return false;
        }

        final LocalDate currentDate = transaction.getLocalDate();
        List<TransactionHistory> transactionHistory = transactionHistoryService.findByYearAndMonth(currentDate);

        //filter history matching the provider LP
        List<TransactionHistory> filtered = transactionHistory
            .stream()
            .filter(t ->
                t.getOriginalTransaction().getDeliveryProvider().getName().equals(providerName) &&
                t.getOriginalTransaction().getPackageSize() == packageSize)
            .collect(Collectors.toList());

        int currentShipmentNumber = filtered.size() + 1;
        return currentShipmentNumber == freeShipmentNumber;
    }

    @Override
    public void process(Transaction transaction) {
        if(!applicable(transaction)) {
            throw new IllegalArgumentException("Refusing to apply discount. Discount not applicable for transaction");
        }

        Price currentPrice = transaction.getDeliveryProvider().getDeliveryMethods().get(PackageSize.L).getPrice();
        Discount discount = new Discount(currentPrice, name);

        transaction.addDiscount(discount);
    }

    @Override
    public String toString() {
        return name;
    }
}
