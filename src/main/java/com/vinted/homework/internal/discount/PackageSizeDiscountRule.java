package com.vinted.homework.internal.discount;

import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.DeliveryMethod;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.service.DeliveryProviderService;

public class PackageSizeDiscountRule extends DiscountRule<Transaction> {
    private final DeliveryProviderService deliveryProviderService;
    private final String discountName = " package size discount";
    private final PackageSize packageSize;

    public PackageSizeDiscountRule(DeliveryProviderService deliveryProviderService, PackageSize packageSize) {
        this.deliveryProviderService = deliveryProviderService;
        this.packageSize = packageSize;
    }

    @Override
    public boolean applicable(Transaction transaction) {
        if(transaction.getPackageSize() != packageSize) {
            return false;
        }

        DeliveryProvider bestOfferProvider = deliveryProviderService.findBestOffer(packageSize);
        return !transaction.getDeliveryProvider().equals(bestOfferProvider);
    }

    @Override
    public void process(Transaction transaction) {
        if(!applicable(transaction)) {
            throw new IllegalArgumentException("Refusing to apply discount. Discount not applicable for transaction");
        }

        DeliveryMethod currentDeliveryMethod = transaction.getDeliveryProvider().getDeliveryMethods().get(packageSize);

        DeliveryProvider bestOffer = deliveryProviderService.findBestOffer(packageSize);
        Price bestPrice = bestOffer.getDeliveryMethods().get(packageSize).getPrice();

        double discountAmount = currentDeliveryMethod.getPrice().getAmount() - bestPrice.getAmount();
        Price discountPrice = new Price(discountAmount);
        Discount discount = new Discount(discountPrice, discountName);

        transaction.addDiscount(discount);
    }

    @Override
    public String toString() {
        return packageSize.name() + discountName;
    }
}
