package com.vinted.homework.internal.processor;

import com.vinted.homework.internal.discount.DiscountRule;
import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;
import com.vinted.homework.service.DiscountService;
import com.vinted.homework.service.TransactionHistoryService;
import com.vinted.homework.service.TransactionService;

import java.util.Comparator;
import java.util.List;

public class TransactionProcessor extends Loggable {

    private final TransactionService transactionService;
    private final TransactionHistoryService transactionHistoryService;
    private final DiscountService discountService;

    public TransactionProcessor(TransactionService transactionService,
                                TransactionHistoryService transactionHistoryService,
                                DiscountService discountService) {
        this.transactionService = transactionService;
        this.transactionHistoryService = transactionHistoryService;
        this.discountService = discountService;
    }

    public void processAll() {
        List<Transaction> transactions = transactionService.findAll();
        if(transactions.isEmpty()) {
            return;
        }

        //Sort transactions by date
        transactions.sort(Comparator.comparing(Transaction::getLocalDate));

        for(Transaction t : transactions) {
            processSingle(t);
        }
    }

    public void processSingle(Transaction transaction) {
        if(!transaction.isValid()) {
            TransactionHistory ignored = new TransactionHistory();
            ignored.setOriginalTransaction(transaction);
            transactionHistoryService.save(ignored);
            return;
        }

        List<DiscountRule> discounts = discountService.availableDiscounts(transaction);

        for(DiscountRule dr : discounts) {
            if(dr.applicable(transaction)) {
                dr.process(transaction);
            }
        }

        List<TransactionHistory> transactionHistoryEntries = transactionHistoryService.findByYearAndMonth(transaction.getLocalDate());
        TransactionHistory completed = PriceProcessor.calculateGrandTotals(transaction, transactionHistoryEntries);
        transactionHistoryService.save(completed);
    }
}
