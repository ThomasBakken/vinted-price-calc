package com.vinted.homework.internal.processor;

import com.vinted.homework.domain.Discount;
import com.vinted.homework.domain.DiscountLineItem;
import com.vinted.homework.domain.Price;
import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.repository.entity.TransactionHistory;

import java.util.List;

public class PriceProcessor extends Loggable {
    private static final double MAX_DISCOUNT_PER_MONTH = 10.00d;

    public static TransactionHistory calculateGrandTotals(Transaction transaction, List<TransactionHistory> transactionHistoryEntries) {
        TransactionHistory completed = new TransactionHistory();
        completed.setOriginalTransaction(transaction);

        Price currentTotalDiscount = new Price(0.0);
        for(Discount d : transaction.getDiscounts()) {
            DiscountLineItem lineItem = new DiscountLineItem(d);
            completed.addDiscountLineItem(lineItem);

            currentTotalDiscount = new Price(currentTotalDiscount.getAmount() + d.getPrice().getAmount());
        }

        //Do not exceed the max discount per month
        double accumulatedDiscount = calculateAccumulatedDiscount(transactionHistoryEntries);
        if(accumulatedDiscount + currentTotalDiscount.getAmount() > MAX_DISCOUNT_PER_MONTH) {
            currentTotalDiscount = new Price(10.00 - accumulatedDiscount);
        }

        double finalAmount = transaction.getOriginalPrice().getAmount() - currentTotalDiscount.getAmount();
        completed.setFinalPrice(new Price(finalAmount));
        completed.setTotalDiscount(currentTotalDiscount);

        return completed;
    }

    private static double calculateAccumulatedDiscount(List<TransactionHistory> transactionHistoryEntries) {
        return transactionHistoryEntries.stream()
            .mapToDouble(entry -> entry.getTotalDiscount().getAmount())
            .sum();
    }
}
