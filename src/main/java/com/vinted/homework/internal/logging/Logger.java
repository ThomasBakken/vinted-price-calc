package com.vinted.homework.internal.logging;

public interface Logger {
    void debug (String msg, Object ... params);
    void info (String msg, Object ... params);
    void warn (String msg, Object ... params);
    void error (String msg, Object ... params);
}
