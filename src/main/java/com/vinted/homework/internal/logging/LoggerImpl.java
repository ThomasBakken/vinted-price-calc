package com.vinted.homework.internal.logging;

import java.time.LocalDateTime;

public class LoggerImpl implements Logger {
    private final String forClass;
    private final LogLevel minLogLevel;

    protected LoggerImpl(String forClass) {
        this(forClass, LogLevel.WARN);
    }

    protected LoggerImpl(String forClass, LogLevel minLogLevel) {
        this.minLogLevel = minLogLevel;
        this.forClass = forClass;
    }

    public void debug(String msg, Object ... params) {
        this.write(LogLevel.DEBUG, msg, params);
    }

    public void info(String msg, Object ... params) {
        this.write(LogLevel.INFO, msg, params);
    }

    public void warn(String msg, Object ... params) {
        this.write(LogLevel.WARN, msg, params);
    }

    public void error(String msg, Object ... params) {
        this.write(LogLevel.ERROR, msg, params);
    }

    private void write(LogLevel logLevel, String msg, Object ... params) {
        if(this.minLogLevel.level <= logLevel.level) {
            System.out.println("[" + logLevel.name() + "] - [" + LocalDateTime.now() + "] " + this.forClass + ": " + String.format(msg, params));
        }
    }

    private enum LogLevel {
        DEBUG(0),
        INFO(1),
        WARN(2),
        ERROR(3);

        private int level;
        LogLevel(int level) {
            this.level = level;
        }
    }
}
