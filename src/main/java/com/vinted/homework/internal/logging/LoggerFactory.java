package com.vinted.homework.internal.logging;

public class LoggerFactory {
    public static Logger getLogger(Class clazz) {
        return new LoggerImpl(clazz.getCanonicalName());
    }
}
