package com.vinted.homework.internal.logging;

public abstract class Loggable {
    protected final Logger logger;
    public Loggable() {
        logger = LoggerFactory.getLogger(this.getClass());
    }
}
