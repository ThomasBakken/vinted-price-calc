package com.vinted.homework.service;

import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.*;

import java.io.File;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Service for dynamically instantiating repositories when run, except from a packaged file (jar)
 * Would normally use a tried and tested dependency injection library.
 *
 * Searches and instantiates all classes that 1) are located in the same package
 * as the {@link com.vinted.homework.repository.Repository repository} class and 2) extends
 * the repository class.
 *
 * Currently does not  search subpackages.
 *
 * Loading classes from within a current jar file is scoped outside of this sample application
 *
 *
 */
public class RepositoryService extends Loggable {
    private Map<Type, Repository> repositories = new HashMap<>();

    public RepositoryService() {
        this.initRepositories();
    }

    public <T> T get(Type type) {
        return (T)repositories.get(type);
    }

    private void initRepositoriesStatic() {
        repositories.put(DeliveryProviderRepository.class, new DeliveryProviderRepository());
        repositories.put(DiscountRuleRepository.class, new DiscountRuleRepository());
        repositories.put(TransactionHistoryRepository.class, new TransactionHistoryRepository());
        repositories.put(TransactionRepository.class, new TransactionRepository());
    }

    private void initRepositories()  {
        boolean isRunningFromJar = detectIfRunningFromJar();
        if(isRunningFromJar) {
            initRepositoriesStatic();
            return;
        }

        ClassLoader classLoader = Repository.class.getClassLoader();
        String basePackage = Repository.class.getPackage().getName();

        //Replace dots with forward slashes to reference file on disk. Not good practice, but gets the job done for now.
        String basePath = basePackage.replaceAll("\\.","/");
        File baseDirectory = new File(classLoader.getResource(basePath).getFile());

        if(!baseDirectory.exists()) {
            logger.error("Failed to resolve base directory for repositories. Base path was %s", basePath);
            return;
        }

        for(File f : baseDirectory.listFiles()) {
            if(f.isDirectory()) {
                logger.info("Skipping directory " + f.getName());
                continue;
            }

            try {
                String className = getClassNameFromFileName(f.getName());

                if(className == null || className.length() < 1) {
                    continue;
                }

                Class c = Class.forName(basePackage + "." + className);

                if(Modifier.isAbstract(c.getModifiers())) {
                    logger.info("Skipping abstract class " + className);
                    continue;
                }

                //Make sure class extends the repository class
                if(Repository.class.isAssignableFrom(c))  {
                    Repository instance = (Repository)c.newInstance();
                    repositories.put(c, instance);
                }

            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                logger.error("Failed to instantiate repository. Exception: %s", e);
            }
        }

        logger.debug("Repository service initialized with " + repositories.size() + " known repos.");
    }

    private String getClassNameFromFileName(String fileName) {
        int postFix = fileName.indexOf(".class");
        if(postFix == -1) {
            logger.info("File " + fileName + " is not a class file. Skipping.");
            return null;
        }

        return fileName.substring(0, postFix);
    }

    private boolean detectIfRunningFromJar() {
        String currentClassName = this.getClass().getName().replace('.', '/');
        String myFileUrl =  this.getClass().getResource("/" + currentClassName + ".class").toString();

        if (myFileUrl.startsWith("jar:")) {
            return true;
        }

        return false;
    }
}
