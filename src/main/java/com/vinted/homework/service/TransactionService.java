package com.vinted.homework.service;

import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.TransactionRepository;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.service.io.TransactionReaderService;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class TransactionService extends Loggable {

    private final TransactionRepository transactionRepository;
    private final TransactionReaderService transactionReaderService;

    public TransactionService(DeliveryProviderService deliveryProviderService,
                              TransactionRepository transactionRepository,
                              String transactionFile) {

        this(transactionRepository, new TransactionReaderService(deliveryProviderService), transactionFile);
    }

    public TransactionService(TransactionRepository transactionRepository,
                              TransactionReaderService transactionReaderService,
                              String transactionFile) {

        this.transactionRepository = transactionRepository;
        this.transactionReaderService = transactionReaderService;

        init(transactionFile);
    }

    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    private void init(String transactionFile) {
        List<Transaction> transactions = Collections.emptyList();

        try {
            if(transactionFile != null) {
                transactions = transactionReaderService.loadFromFile(transactionFile);
            } else {
                transactions = transactionReaderService.loadFromResource();
            }

        } catch (IOException e) {
            logger.error("Failed to read transactions from resource file. Exception: %s", e);
        }

        if(transactions.isEmpty()) {
            logger.warn("No transactions found");
            return;
        }

        logger.debug("Found %d transactions", transactions.size());
        transactionRepository.save(transactions);
    }
}
