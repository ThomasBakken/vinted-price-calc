package com.vinted.homework.service;

import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.TransactionHistoryRepository;
import com.vinted.homework.repository.entity.TransactionHistory;

import java.time.LocalDate;
import java.util.List;

public class TransactionHistoryService extends Loggable {
    private final TransactionHistoryRepository transactionHistoryRepository;

    public TransactionHistoryService(TransactionHistoryRepository transactionHistoryRepository) {
        this.transactionHistoryRepository = transactionHistoryRepository;
    }

    public void save(TransactionHistory entry) {
        transactionHistoryRepository.save(entry);
    }

    public List<TransactionHistory> findByYearAndMonth(LocalDate currentDate) {
        return transactionHistoryRepository.findByYearAndMonth(currentDate);
    }

    public List<TransactionHistory> findAll() {
        return transactionHistoryRepository.findAll();
    }
}
