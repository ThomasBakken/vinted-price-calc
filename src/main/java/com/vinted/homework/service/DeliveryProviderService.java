package com.vinted.homework.service;

import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.DeliveryMethod;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.internal.logging.Loggable;
import com.vinted.homework.repository.DeliveryProviderRepository;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.service.io.DeliveryProviderReaderService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class DeliveryProviderService extends Loggable {
    public static final String DEFAULT_PROVIDER_FILENAME = "/providers.resource";

    private final DeliveryProviderReaderService deliveryProviderReaderService;
    private final DeliveryProviderRepository deliveryProviderRepository;

    public DeliveryProviderService(DeliveryProviderRepository deliveryProviderRepository) {
        this(new DeliveryProviderReaderService(), deliveryProviderRepository);
    }

    public DeliveryProviderService(DeliveryProviderReaderService deliveryProviderReaderService, DeliveryProviderRepository deliveryProviderRepository) {
        this.deliveryProviderReaderService = deliveryProviderReaderService;
        this.deliveryProviderRepository = deliveryProviderRepository;

        init();
    }

    public List<DeliveryProvider> findAll() {
        return deliveryProviderRepository.findAll();
    }

    public Optional<DeliveryProvider> findByName(String providerName) {
        return deliveryProviderRepository.findAll().stream()
            .filter(provider -> provider.getName().equals(providerName))
            .findFirst();
    }

    public DeliveryProvider findBestOffer(PackageSize packageSize) {
        DeliveryProvider bestOffer = null;
        Price bestPrice = new Price(Double.MAX_VALUE);

        for(DeliveryProvider dp : findAll()) {
            DeliveryMethod method = dp.getDeliveryMethods().get(packageSize);
            if(method == null) continue;

            if(bestPrice.moreThan(method.getPrice())) {
                bestPrice = method.getPrice();
                bestOffer = dp;
            }
        }

        return bestOffer;
    }

    private void init() {
        try {
            List<DeliveryProvider> deliveryProviders = deliveryProviderReaderService.load(DeliveryProviderService.DEFAULT_PROVIDER_FILENAME);
            deliveryProviderRepository.save(deliveryProviders);
        } catch (IOException e) {
            logger.error("Failed to load default delivery providers. Exception: %s ", e);
        }
    }
}
