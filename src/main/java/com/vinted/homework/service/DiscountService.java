package com.vinted.homework.service;

import com.vinted.homework.internal.discount.DiscountRule;
import com.vinted.homework.repository.DiscountRuleRepository;
import com.vinted.homework.repository.entity.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class DiscountService {
    private final DiscountRuleRepository discountRuleRepository;

    public DiscountService(DiscountRuleRepository discountRuleRepository) {
        this.discountRuleRepository = discountRuleRepository;

    }

    public void addDiscountRule(DiscountRule discountRule) {
        this.discountRuleRepository.save(discountRule);
    }

    public List<DiscountRule> availableDiscounts(final Transaction transaction) {
        List<DiscountRule> discounts = discountRuleRepository.findAll();

        return discounts.stream().filter(discount -> discount.applicable(transaction))
            .collect(Collectors.toList());
    }
}
