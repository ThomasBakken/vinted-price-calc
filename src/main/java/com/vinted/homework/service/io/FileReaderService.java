package com.vinted.homework.service.io;

import com.vinted.homework.internal.logging.Loggable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class FileReaderService extends Loggable {
    private final String defaultDelimiter = "\\z"; //EOF delimiter
    private final Charset defaultCharset = StandardCharsets.UTF_8;

    protected String readResourceFile(String path) throws IOException {
        InputStream inputStream  = getClass().getResourceAsStream(path);
        if (inputStream == null) {
            throw new IOException("Resource "  + path + " not found");
        }

        String contents = readUntilEnd(inputStream);
        inputStream.close();;

        return contents;
    }

    protected String readFile(String path) throws IOException {
        File file = new File(path);
        if(!file.exists()) {
            throw new IOException("Input file " + file.getName() + " does not exist!");
        }

        FileInputStream in = new FileInputStream(file);
        String contents = readUntilEnd(in);
        in.close();

        return contents;
    }

    private String readUntilEnd(InputStream in) throws IOException {
        if(in == null) {
            throw new IOException("Input stream is null");
        }

        String contents;
        try (Scanner scanner = new Scanner(in, defaultCharset.name()) ) {
            contents = scanner.useDelimiter(this.defaultDelimiter).next();
        }

        return contents;
    }
}
