package com.vinted.homework.service.io;

import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.DeliveryMethod;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.DeliveryProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeliveryProviderReaderService extends FileReaderService {

    /**
     * Expected format of contents
     *
     * <provider name>\r\n
     * <package size 1> <package size 2> ... <package size n>\r\n
     * <price size 1> <price size 2> ... <price size n>\r\n
     * <currency identifier>\r\n
     * -\r\n
     *
     */
    public static final String PROVIDER_DELIMITER = "\\n-\\n";
    public static final String ENTRY_DELIMITER = "\\n";
    private final int  expectedLinesPerProvider = 4;


    public List<DeliveryProvider> parse(String contents) {
        return  verifyAndParse(contents);
    }

    /**
     * Reads the content of the provided file, and parses the contents according to the given format.
     * @param filename the file to read
     */
    public List<DeliveryProvider> load(String filename) throws IOException {
        String contents = readResourceFile(filename);
        if(contents == null || contents.trim().length() < 1) {
            return Collections.emptyList();
        }

        return parse(contents);
    }

    private List<DeliveryProvider> verifyAndParse(String contents) {
        //Just in case we are dealing with files saved using windows line endings
        contents = contents.replaceAll("\\r", "");

        ArrayList<DeliveryProvider> deliveryProviders = new ArrayList<>();
        String name, packageSizes, packagePrices, currency;
        String[] lines;

        for(String entry : contents.split(PROVIDER_DELIMITER)) {
            lines = entry.split(ENTRY_DELIMITER);

            if(lines.length != expectedLinesPerProvider) {
                logger.warn("Expected %d but found %d. Skipping", expectedLinesPerProvider, lines.length);
                continue;
            }

            name = lines[0];
            packageSizes = lines[1];
            packagePrices = lines[2];
            currency = lines[3];
            deliveryProviders.add(createProvider(name, packageSizes, packagePrices, currency));
        }

        return deliveryProviders;
    }

    private DeliveryProvider createProvider(String name, String packageSizes, String packagePrices, String currency) {
        String[] sizes = packageSizes.split(" ");
        String[] prices = packagePrices.split(" ");

        List<DeliveryMethod> deliveryMethods = createDeliveryMethods(sizes, prices, currency);
        return new DeliveryProvider(name, deliveryMethods);
    }

    private List<DeliveryMethod> createDeliveryMethods(String[] packageSizes, String[] packagePrices, String currency) {
        List<DeliveryMethod> deliveryMethods = new ArrayList<>();
        if(packageSizes.length != packagePrices.length) {
            System.out.println("WARN: Length mismatch of package size and package price. Size: " + packageSizes.length + ", Price: " + packagePrices.length);
        }

        int length = Math.max(packageSizes.length, packagePrices.length);

        int canonicalAmount;
        double amount;
        PackageSize size;
        DeliveryMethod dm;
        Price price;
        for(int i = 0; i < length; i++) {
            canonicalAmount = Integer.parseInt(packagePrices[i]);
            amount = canonicalAmount / 100.0;

            price = new Price(amount, currency);
            size = PackageSize.valueOf(packageSizes[i]);

            dm = new DeliveryMethod(price, size);
            deliveryMethods.add(dm);
        }

        return deliveryMethods;
    }

}
