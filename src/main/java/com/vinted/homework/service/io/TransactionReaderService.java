package com.vinted.homework.service.io;

import com.vinted.homework.domain.Price;
import com.vinted.homework.domain.delivery.PackageSize;
import com.vinted.homework.repository.entity.DeliveryProvider;
import com.vinted.homework.repository.entity.Transaction;
import com.vinted.homework.service.DeliveryProviderService;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class TransactionReaderService extends FileReaderService {
    public static final String DEFAULT_TRANSACTION_RESOURCE_FILE = "/transactions.resource";
    private final FileReaderService fileReaderService;
    private final DeliveryProviderService deliveryProviderService;

    private final String TRANSACTION_DELIMITER = "\\n";
    private final String INPUT_DELIMITER = "\\s";
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public TransactionReaderService(DeliveryProviderService deliveryProviderService) {
        this(new FileReaderService(), deliveryProviderService);
    }

    public TransactionReaderService(FileReaderService fileReaderService, DeliveryProviderService deliveryProviderService) {
        this.fileReaderService = fileReaderService;
        this.deliveryProviderService = deliveryProviderService;
    }

    public List<Transaction> loadFromFile(String filename) throws IOException {
        String contents = fileReaderService.readFile(filename);

        return parseTransactions(contents);
    }

    public List<Transaction> loadFromResource() throws IOException{
        String resourceFile = TransactionReaderService.DEFAULT_TRANSACTION_RESOURCE_FILE;
        String contents = fileReaderService.readResourceFile(resourceFile);

        return parseTransactions(contents);
    }

    public List<Transaction> parseTransactions(String raw) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        String date, size, providerName;
        String[] details;

        //In case of windows based line-endings, remove carriage return char
        raw = raw.replaceAll("\\r", "");

        long originalOrder = 0;
        for(String entry : raw.split(TRANSACTION_DELIMITER)) {
            details = entry.split(INPUT_DELIMITER);

            if(details.length != 3) {
                //Invalid line entry. Create a placeholder
                Transaction placeholder = createPlaceholder(entry);
                placeholder.setLineNumber(originalOrder++);
                transactions.add(placeholder);
                continue;
            }

            date = details[0];
            size = details[1];
            providerName = details[2];

            Transaction t = createTransaction(date, size, providerName);
            if(t == null) {
                //Parsing failed. Create a placeholder instead.
                Transaction placeholder = createPlaceholder(entry);
                placeholder.setLineNumber(originalOrder++);
                transactions.add(placeholder);
                continue;
            }

            t.setLineNumber(originalOrder++);
            t.setRaw(entry);
            transactions.add(t);
        }

        return transactions;
    }

    private Transaction createPlaceholder(String rawInput) {
        Transaction transaction = new Transaction();
        transaction.setRaw(rawInput);
        transaction.setValid(false);

        //all invalid transactions will have a date in the future, and will appear at the end of the list.
        transaction.setLocalDate(LocalDate.MAX);
        return transaction;
    }

    public  Transaction createTransaction(String date, String packageSize, String providerName) {
        LocalDate parsedDate;
        PackageSize parsedPackageSize;
        DeliveryProvider provider;

        try {
            parsedDate = LocalDate.parse(date, formatter);

            //Will cause illegal argument exception on unkown values
            parsedPackageSize = PackageSize.valueOf(packageSize);

            //In case provider is unknown, we cannot handle the transaction and mark it as invalid for our calculator.
            provider = deliveryProviderService.findByName(providerName).get();
            if(provider == null) {
                return null;
            }

        } catch(DateTimeParseException | IllegalArgumentException e) {
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.setDeliveryProvider(provider);
        transaction.setLocalDate(parsedDate);
        transaction.setPackageSize(parsedPackageSize);

        Price price = provider.getDeliveryMethods().get(parsedPackageSize).getPrice();
        transaction.setOriginalPrice(price);

        transaction.setValid(true);
        return transaction;
    }
}
