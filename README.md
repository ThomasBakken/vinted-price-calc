# VINTED shipment discount calculator

A shipment discount calculator that takes a list of shipment orders as input and outputs
their final price with discount subtracted (if any) along with the amount of discount.  

All entries are read ahead of processing, and any entry that does not conform 
with the format given in the task description will be ignored as far as calculation goes,
and all inputs will be output in the order they were received.

This includes  

- Invalid date format
- Unkown provider name
- Unknown package size

**Test**  

    mvn clean test

**Package**  

    mvn clean package

**Run**

    java -jar discount-calc.jar <transaction-file>
    
    
## Requirements
1. Java 8
2. Maven v.3 or higher
